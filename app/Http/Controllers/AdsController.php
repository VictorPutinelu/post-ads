<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ad;

class AdsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Conta gli users e se il risultato è 0 ne genera 10
        // TODO: Rimuovere la creazione automatica degli users e degli Ads
        $users = \App\User::count();

        if ($users === 0) {
            factory(\App\User::class, 10)->create();
        } else {
            $users = \App\User::all();
        }

        $ads = Ad::count();

        if ($ads === 0) {
            factory(Ad::class, 10)->create();
        }

        // Ordina per Updated_at DESC
        $ads = \App\Ad::where('archived', 0)->orderBy('updated_at', 'desc')->paginate(5);

        // Messaggio di errore nel caso non vengano trovati Ads
        if ($ads->count() == 0) {
            $request->session()->flash('error', 'No resources were found!');
        }

        return view('ads.index',
            [
                'title' => 'Ads',
                'ads' => $ads
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ad = new Ad;
        
        // $ad = factory(Ad::class)->make();

        return view('ads.create', [
            'title' => 'Post a new Ad',
            'ad' => $ad
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|between:1,255|regex:/(^[A-Za-z0-9 ]+$)+/',
            'description' => 'required',
            'price' => 'required|numeric|between:0,999999.99'
        ]);

        $ad = Ad::create([
            'title' => request('title'),
            'description' => request('description'),
            'price' => request('price'),
            'user_id' => \Auth::id(),
            // TODO: Sostituire i senguenti dati con dati reali
            'category_id' => 1,
            'address_id' => 1,
            'status_id' => 1
        ]);

        return redirect('my-ads')
            ->with('success', 'The Ad: "'. $ad->title .'" was created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad) // Route Model Binding
    {
        return view('ads.show', compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        if (\Auth::id() !== $ad->user_id) {
            // TODO: sostituire questo errore con 403 o con il middleware
            abort(404);
        }

        $title = 'Edit an Ad';
        return view('ads.edit', compact('ad', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ad = Ad::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|between:1,255|regex:/(^[A-Za-z0-9 ]+$)+/',
            'description' => 'required',
            'price' => 'required|numeric|between:0,999999.99'
        ]);

        $ad->title = request('title');
        $ad->description = request('description');
        $ad->price = request('price');
        $ad->updated_at = now();

        if (!$ad->save()) {
            $errors = $ad->getErrors();
            return redirect()
                    ->action('AdsController@edit')
                ->with('errors', $errors)
                ->withInput();
        }
        return redirect('my-ads')
            ->with('success', 'The Ad: "'. $ad->title .'" was updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ad::findOrFail($id);

        if (\Auth::id() !== $ad->user_id) {
            // TODO: sostituire questo errore con 403 o con il middleware
            abort(404);
        }
        $title = $ad->title;

        $ad->delete();

        return redirect('my-ads')
            ->with('success', 'The Ad: "'. $title .'" was deleted.');
    }

    public function archived(Request $request)
    {
        $ads = Ad::archived()->paginate(5);

        if ($ads->count() == 0) {
            $request->session()->flash('error', 'No resources were found!');
        }

        return view('ads.archive',
            [
                'title' => 'Archived Ads',
                'ads' => $ads
            ]
        );
    }

    public function myAds(Request $request)
    {
        // TODO: Sostituire la seguente query con il metodo $user->ads(). La variabile $user è un instanza di User
        $ads = \App\Ad::where('user_id', \Auth::id())
            ->where('archived', 0)
            ->orderBy('updated_at', 'DESC')
            ->paginate(5);

        return view('ads.archive', [
                'title' => 'My Ads',
                'ads' => $ads
        ]);
    }

    public function archive($id)
    {
        $ad = Ad::findOrFail($id);

        if (\Auth::id() !== $ad->user_id) {
            // TODO: sostituire questo errore con 403 o con il middleware
            abort(404);
        }

        if ($ad->archived === 0)
        {
            $ad->archived = 1;
        } else {
            $ad->archived = 0;
        }
        $ad->save();
        return redirect('my-ads')
            ->with('success', 'The Ad: "'. $ad->title .'" was archived.');
    }
}
