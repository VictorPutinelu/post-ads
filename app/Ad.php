<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $fillable = ['title', 'description', 'price', 'user_id', 'category_id', 'address_id', 'status_id'];
    /**
     * Ads archiviate dell'utente loggato
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeArchived($query)
    {
        return $query->where('archived', 1)->where('user_id', \Auth::id());
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAt()
    {
        return $this->formatDate('created_at');
    }

    public function getUpdatedAt()
    {
        return $this->formatDate('updated_at');
    }

    private function formatDate($attribute)
    {
        return date('d-m-Y H:i:s', strtotime($this->attributes[$attribute]));
    }
}