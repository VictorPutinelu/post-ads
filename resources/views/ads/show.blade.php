@extends('layouts.app')

@section('content')
    <div class="container">

        <h1> {{ $ad->title }} </h1>
        @if (\Auth::id() === $ad->user_id)
        <div class="form-inline mb-2">
            <a href="{{ route("ads.edit", $ad->id) }}">
                <button class="btn btn-primary mr-2">Edit</button>
            </a>
            <a href="{{ route("ads.archive", $ad->id) }}">
                @if ($ad->archived === 0)
                    <button class="btn btn-warning mr-2">Archive</button>
                @else
                    <button class="btn btn-warning mr-2">Remove from Archive</button>
                @endif
            </a>

            <form action="/ads/{{ $ad->id }}" method="POST" class="form-group" onsubmit="return confirm('Are you sure?')">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
        @endif

        <table class="table table-striped">
            <tr>
                <th>ID</th>
                <td>{{ $ad->id }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ $ad->description }}</td>
            </tr>
            <tr>
                <th>Price</th>
                <td>{{ $ad->price }}</td>
            </tr>
            <tr>
                <th>Created At</th>
                <td>{{ $ad->getCreatedAt() }}</td>
            </tr>
            <tr>
                <th>Updated At</th>
                <td>{{ $ad->getUpdatedAt() }}</td>
            </tr>
            <tr>
                <th>Views</th>
                <td>{{ $ad->views }}</td>
            </tr>
            <tr>
                <th>Archived</th>
                <td>{{ $ad->archived ? 'true' : 'false' }}</td>
            </tr>
            <tr>
                <th>Created by</th>
                <td>{{ $ad->user->name }}</td>
            </tr>
{{--             <tr>
                <th>Category ID</th>
                <td>{{ $ad->category_id }}</td>
            </tr>
            <tr>
                <th>Address ID</th>
                <td>{{ $ad->address_id }}</td>
            </tr>
            <tr>
                <th>Status ID</th>
                <td>{{ $ad->status_id }}</td>
            </tr> --}}

        </table>

    </div>
@endsection