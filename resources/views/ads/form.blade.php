@if (!$ad->exists)
<form method="POST" action="/ads">
@else
<form method="POST" action="/ads/{{ $ad->id }}">
{{ method_field('PUT') }}
@endif
	{{ csrf_field() }}
	<div class="form-group">
		<label for="title">Title</label>
		<input type="text" name="title" id="title" class="form-control" value="{{ $ad->title }}" required>
	</div>

	<div class="form-group">
		<label for="description">Description</label>
		<textarea name="description" id="description" class="form-control" required>{{ $ad->description }}</textarea>
	</div>

	<div class="form-group">
		<label for="price">Price</label>
		<input type="number" step="0.01" min="0" max="999999.99" name="price" id="price" class="form-control" value="{{ $ad->price }}" required>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-primary">Post</button>		
	</div>
</form>