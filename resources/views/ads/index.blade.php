@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container">
        <h1 class="display-3 text-center">{{ $title }}</h1>
        @include('partials.alerts')

        @foreach ($ads as $ad)
            <article class="pt-3 pb-3 border-bottom">
                <h2>
                    <a href="{{ route('ads.show', ['id' => $ad->id]) }}">{{ $ad->title }}</a>
                </h2>
                <p class="h5">Price: {{ $ad->price }} €</p>
                <p title="{{ $ad->getUpdatedAt() }}">
                    {{ $ad->updated_at->toFormattedDateString() }}
                     - 
                    Created by: {{ $ad->user->name }}
                </p>
                <p>
                    {{ $ad->description }}
                </p>
            </article>
        @endforeach

        {{ $ads->links() }}
    </div>
@endsection