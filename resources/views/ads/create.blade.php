@extends('layouts.app')

@section('title', $title)

@section('content')

    <div class="container">
        <h1>{{ $title }}</h1>
        
        @include('partials.alerts')
        
        @include('ads.form')

    </div>    
@endsection