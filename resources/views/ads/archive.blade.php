@extends('layouts.app')

@section('title', $title)

@section('content')
    <div class="container">
        <h1>{{ $title }}</h1>

        @include('partials.alerts')

        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Price</th>
                <th>Updated at</th>
                {{--<th>Created by</th> --}}
                <th>Actions</th>
            </tr>
            </thead>
            @foreach ($ads as $ad)
                <tr class="table-tr">
                    <td>{{ $ad->id }}</td>
                    <td>
                        <a href="{{ route('ads.show', ['id' => $ad->id]) }}">{{ $ad->title }}</a>
                    </td>
                    <td>{{ $ad->price }}</td>
                    <td>{{ $ad->getUpdatedAt() }}</td>
                    <td>
                        <div class="form-inline">
                            <a href="{{ route("ads.edit", $ad->id) }}">
                                <button class="btn btn-primary mr-2">Edit</button>
                            </a>

                            <form action="/ads/{{ $ad->id }}" method="POST" class="form-group" onsubmit="return confirm('Are you sure?')">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $ads->links() }}
    </div>
@endsection