<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->decimal('price', 8, 2); // Valore massimo 999 999.99
            $table->boolean('archived')->default(false);
            $table->unsignedInteger('views')->default(0); // Numero di visualizzazioni
            $table->timestamps();

            $table->unsignedInteger('user_id');
            // Tabelle ancora da creare
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('address_id');
            $table->unsignedInteger('status_id');

            // Indexes
            // Example: $table->unique(['title']);

            // Foreign Keys

            // User
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('restrict')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
