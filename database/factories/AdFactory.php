<?php

use Faker\Generator as Faker;

$factory->define(App\Ad::class, function (Faker $faker) {
    return [
        'title' => $faker->word(20),
        'description' => $faker->text(500),
        'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 1000.00),
        'archived' => false, // Valore predefinito: false
        'views' => $faker->numberBetween($min = 1, $max = 50000),
        'created_at' => $faker->dateTimeBetween('-3 years', 'now'),
        'updated_at' => function (array $ad) {
            return $ad['created_at'];
        },
        'user_id' => App\User::all()->random()->id,
        'category_id' => $faker->numberBetween($min = 1, $max = 50),
        'address_id' => $faker->numberBetween($min = 1, $max = 50),
        'status_id' => $faker->numberBetween($min = 1, $max = 50),
    ];
});
