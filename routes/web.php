<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return redirect('/ads');
});

Auth::routes();

// TODO: Rindirizza dopo aver visualizzato la pagina 'home'
Route::get('/home', 'HomeController@index')->name('home');

// Ads
/*
	Se non è presente la route GET '/ads' qualora la si cerci di raggiungere di si verifica il seguente errore: 'MethodNotAllowedHttpException'
	Source: https://github.com/laravel/framework/issues/24745
	// Route::get('/', 'AdsController@index')->name('ads.index');
*/
/*
Route::get('ads', function(){
	// return redirect('/');
	abort(404);
})->name('ads.index');
*/
Route::get('my-ads', 'AdsController@myAds')->name('my-ads');
Route::get('ads/{ad}/archive', 'AdsController@archive')->name('ads.archive');
Route::get('ads/archived', 'AdsController@archived')->name('ads.archived');

Route::resource('ads', 'AdsController', [
	// Esclude le seguenti azioni
	// 'except' => ['index']
]);
